﻿using System;

namespace AngleCalculator
{
    /// <summary>
    /// Represents Units
    /// </summary>
    public struct Units
    {
        /// <summary>
        /// Right Angle
        /// </summary>
        public const double RightAngle = Math.PI * 0.5;

        /// <summary>
        /// Straight Angle
        /// </summary>
        public const double StraightAngle = Math.PI;

        /// <summary>
        /// Full Angle
        /// </summary>
        public const double FullAngle = Math.PI * 2.0;

        /// <summary>
        /// Radian By Degrees (PI/180)
        /// </summary>
        public const double RadianByDegrees = StraightAngle / 180.0;

        /// <summary>
        /// Degree By Radians (180/PI)
        /// </summary>
        public const double DegreeByRadians = 180.0 / StraightAngle;

        /// <summary>
        /// Full Angle in Degree
        /// </summary>
        public const double FullAngleDegree = 360.0;

        /// <summary>
        /// Right Angle in Degree
        /// </summary>
        public const double RightAngleDegree = 90.0;

        /// <summary>
        /// Straight Angle in Degree
        /// </summary>
        public const double StraightAngleDegree = 180.0;

        /// <summary>
        /// Initial Degree
        /// </summary>
        public const double InitDegree = 0.0;

        /// <summary>
        /// Direction Ratio Degree
        /// </summary>
        public const double DirectionRatioDegree = 450.0;

    }
}
