﻿using System;

namespace AngleCalculator
{
    /// <summary>
    /// Represents Radian unit.
    /// </summary>
    public class Radian : Angle
    {
        #region Constructor

        /// <summary>
        /// Radian constructor accepts double.
        /// </summary>
        /// <param name="value"></param>
        public Radian(double value) : base(value)
        {
            Value = Reduce(value);
        }

        #endregion

        #region Angle abstractions

        /// <summary>
        /// Divides a degree by a angle. 
        /// </summary>
        /// <param name="divisor">Divisor source double.</param>
        /// <returns>Result of the division.</returns>
        public override Angle Divide(double divisor)
        {
            if (divisor == 0) throw new DivideByZeroException($"Divisor should be greater than 0.");
            return new Radian(Value / divisor);
        }

        /// <summary>
        /// Multiplies a degree by an angle value.
        /// </summary>
        /// <param name="multiplicant">Multiplicant source double.</param>
        /// <returns>Result of the multiplication in degree.</returns>
        public override Angle Multiply(double multiplicant) => new Radian(Value * multiplicant);

        /// <summary>
        /// Adds two angles. 
        /// </summary>
        /// <param name="angle">Source angle.</param>
        /// <returns>Result of the addition.</returns>
        public override Angle Add(Angle angle) => new Radian(Value + ToRadian(angle).Value);
        /// <summary>
        /// Subtracts a angle from a angle.  
        /// </summary>
        /// <param name="angle">Source angle.</param>
        /// <returns>Result of the subtraction.</returns>
        public override Angle Subtract(Angle angle) => new Radian(Value - ToRadian(angle).Value);

        /// <summary>
        /// Indicates whether a specified Angle is less than another specified Angle.
        /// </summary>
        /// <param name="angle">The angle to compare.</param>
        /// <returns>true if the value of firstAngle is less than the value of secondAngle; otherwise, false.</returns>
        public override bool IsLessThan(Angle angle) => Value < ToRadian(angle).Value;

        /// <summary>
        /// Indicates whether a specified Angle is less than or equal to another specified Angle.
        /// </summary>
        /// <param name="angle">The angle to compare.</param>
        /// <returns>true if the value of firstAngle is less than or equal to the value of secondAngle; otherwise, false.</returns>
        public override bool IsLessThanOrEqual(Angle angle) => Value <= ToRadian(angle).Value;

        /// <summary>
        /// Indicates whether a specified Angle is greater than another specified Angle.
        /// </summary>
        /// <param name="angle">The angle to compare.</param>
        /// <returns>true if the value of firstAngle is greater than the value of secondAngle; otherwise, false.</returns>
        public override bool IsGreaterThan(Angle angle) => Value > ToRadian(angle).Value;

        /// <summary>
        /// Indicates whether a specified Angle is greater than or equal to another specified Angle.
        /// </summary>
        /// <param name="angle">The angle to compare.</param>
        /// <returns>true if the value of firstAngle is greater than or equal to the value of secondAngle; otherwise, false.</returns>
        public override bool IsGreaterThanOrEqual(Angle angle) => Value >= ToRadian(angle).Value;

        /// <summary>
        /// Indicates a specified Angle is equal to another specified Angle.
        /// </summary>
        /// <param name="angle">The angle to compare.</param>
        /// <returns>true if the value of firstAngle is equal to the value of secondAngle; otherwise, false.</returns>
        public override bool IsEqual(Angle angle) => Value == ToRadian(angle).Value;

        /// <summary>
        /// Indicates a specified Angle is not equal to another specified Angle.
        /// </summary>
        /// <param name="angle">The angle to compare.</param>
        /// <returns>true if the value of firstAngle is equal to the value of secondAngle; otherwise, false.</returns>
        public override bool IsNotEqual(Angle angle) => Value != ToRadian(angle).Value;

        /// <summary>
        /// Remainder after division of one angle by another.
        /// </summary>
        /// <param name="angle">The divisor angle.</param>
        /// <returns>Returns remainder after division of divident angle by divisor angle.</returns>
        public override double Mod(Angle angle) => Value % ToRadian(angle).Value;

        /// <summary>
        /// Returns the angle whose sine is the specified number.
        /// </summary>
        /// <param name="value">A number representing a sine, where value must be greater than or equal to -1, but less than or equal to 1.</param>
        public override Angle Asin(double value)
        {
            if (value < -1.0 || value > 1.0)
                throw new ArgumentOutOfRangeException(nameof(value), value, "Argument must be greater or equal to -1.0 and less or equal to 1.0.");
            return new Radian(Math.Asin(value));
        }


        /// <summary>
        /// Returns the angle whose cosine is the specified number.
        /// </summary>
        /// <param name="value">A number representing a cosine, where value must be greater than or equal to -1, but less than or equal to 1.</param>
        /// <returns>The angle whose cosine is the specified number.</returns>
        public override Angle Acos(double value)
        {
            if (value < -1.0 || value > 1.0)
                throw new ArgumentOutOfRangeException(nameof(value), value, "Argument must be greater or equal to -1.0 and less or equal to 1.0.");

            return new Radian(Math.Acos(value));
        }

        /// <summary>
        /// Returns the angle whose tangent is the specified number.
        /// </summary>
        /// <param name="value">A number representing a tangent.</param>
        /// <returns>The angle whose tangent is the specified number.</returns>
        public override Angle Atan(double value) => new Radian(Math.Atan(value));

        /// <summary>
        /// To Double value.
        /// </summary>
        /// <returns>Returns double value of Radian.</returns>
        public override double ToDouble()
        {
            return Value;
        }
        #endregion

        #region Radian Utils

        /// <summary>
        /// Convert from angle to radian
        /// </summary>
        /// <param name="angle">Source angle.</param>
        /// <returns>Result in radian.</returns>
        public Angle ToRadian(Angle angle)
        {
            dynamic dynamicAngle = angle;
            return (Radian)dynamicAngle;

            //new RadianFactory(angle).Create();
        }
        /// <summary>
        /// Reduce double by FullAngle Units.
        /// </summary>
        /// <param name="radians">double value.</param>
        /// <returns>Reduced double.</returns>
        public static double Reduce(double radians)
        {
            var reduced = radians % Units.FullAngle;
            return reduced >= 0 ? reduced : reduced + Units.FullAngle;
        }

        #endregion

        #region Cast

        /// <summary>
        /// Implicitly convert Double to Radian.
        /// </summary>
        /// <param name="value">Double value.</param>
        public static implicit operator Radian(Double value)
        {
            return new Radian(Reduce(value));
        }

        /// <summary>
        /// Implicitly convert Radian to Double.
        /// </summary>
        /// <param name="radian">Radian object.</param>
        public static implicit operator Double(Radian radian)
        {
            return radian.ToDouble();
        }

        /// <summary>
        /// Explicitly convert Radian to Degree.
        /// </summary>
        /// <param name="radian">Radian object.</param>
        public static explicit operator Degree(Radian radian)
        {
            return new Degree(radian.Value * Units.DegreeByRadians);
        }

        #endregion
    }
}
