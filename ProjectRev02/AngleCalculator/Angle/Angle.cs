﻿using System;

namespace AngleCalculator
{
    /// <summary>
    /// Represents an angle. 
    /// </summary>
    public abstract class Angle : IEquatable<Angle>
    {

        #region Properties
        public double Value { get; set; }

        #endregion

        #region Constructor
        public Angle(double value) => Value = value;

        #endregion

        #region Multiplication

        /// <summary>
        /// Multiplies an angle by an angle value. 
        /// </summary>
        /// <param name="multiplier">Multiplier source angle.</param>
        /// <param name="multiplicant">Multiplicant source double.</param>
        /// <returns>Result of the multiplication in angle.</returns>
        public static Angle operator *(Angle multiplier, double multiplicant) => multiplier.Multiply(multiplicant);

        /// <summary>
        /// Multiplies an angle by an angle value.
        /// </summary>
        /// <param name="multiplicant">Multiplicant source double.</param>
        /// <returns>Result of the multiplication in angle.</returns>
        public abstract Angle Multiply(double multiplicant);

        #endregion

        #region Division

        /// <summary>
        /// Divides a angle by a angle. 
        /// </summary>
        /// <param name="dividend">Dividend angle.</param>
        /// <param name="divisor">Divisor angle.</param>
        /// <returns>Result of the division.</returns>
        public static Angle operator /(Angle dividend, double divisor) => dividend.Divide(divisor);

        /// <summary>
        /// Divides a angle by a angle. 
        /// </summary>
        /// <param name="divisor">Divisor in double.</param>
        /// <returns>Result of the division.</returns>
        public abstract Angle Divide(double divisor);

        #endregion

        #region Addition

        /// <summary>
        /// Adds two angles. 
        /// </summary>
        /// <param name="left">Source angle.</param>
        /// <param name="right">Source angle.</param>
        /// <returns>Result of the addition.</returns>
        public static Angle operator +(Angle left, Angle right) => left.Add(right);

        /// <summary>
        /// Adds two angles. 
        /// </summary>
        /// <param name="right">Source angle.</param>
        /// <returns>Result of the addition.</returns>
        public abstract Angle Add(Angle right);

        #endregion

        #region Subtraction

        /// <summary>
        /// Subtracts a angle from a angle.  
        /// </summary>
        /// <param name="left">Source angle.</param>
        /// <param name="right">Source angle.</param>
        /// <returns>Result of the subtraction.</returns>
        public static Angle operator -(Angle left, Angle right) => left.Subtract(right);

        /// <summary>
        /// Subtracts a angle from a angle.  
        /// </summary>
        /// <param name="right">Source angle.</param>
        /// <returns>Result of the subtraction.</returns>
        public abstract Angle Subtract(Angle right);

        #endregion

        #region Comparison implementation

        /// <summary>
        /// Indicates whether a specified Angle is less than another specified Angle.
        /// </summary>
        /// <param name="firstAngle">The first angle to compare.</param>
        /// <param name="secondAngle">The second angle to compare.</param>
        /// <returns>true if the value of firstAngle is less than the value of secondAngle; otherwise, false.</returns>
        public static bool operator <(Angle firstAngle, Angle secondAngle) => firstAngle.IsLessThan(secondAngle);

        /// <summary>
        /// Indicates whether a specified Angle is less than another specified Angle.
        /// </summary>
        /// <param name="angle">The angle to compare.</param>
        /// <returns>true if the value of firstAngle is less than the value of secondAngle; otherwise, false.</returns>
        public abstract bool IsLessThan(Angle angle);

        /// <summary>
        /// Indicates whether a specified Angle is less than or equal to another specified Angle.
        /// </summary>
        /// <param name="firstAngle">The first angle to compare.</param>
        /// <param name="secondAngle">The second angle to compare.</param>
        /// <returns>true if the value of firstAngle is less than or equal to the value of secondAngle; otherwise, false.</returns>
        public static bool operator <=(Angle firstAngle, Angle secondAngle) => firstAngle.IsLessThanOrEqual(secondAngle);

        /// <summary>
        /// Indicates whether a specified Angle is less than or equal to another specified Angle.
        /// </summary>
        /// <param name="angle">The angle to compare.</param>
        /// <returns>true if the value of firstAngle is less than or equal to the value of secondAngle; otherwise, false.</returns>
        public abstract bool IsLessThanOrEqual(Angle angle);


        /// <summary>
        /// Indicates whether a specified Angle is greater than another specified Angle.
        /// </summary>
        /// <param name="firstAngle">The first angle to compare.</param>
        /// <param name="secondAngle">The second angle to compare.</param>
        /// <returns>true if the value of firstAngle is greater than the value of secondAngle; otherwise, false.</returns>
        public static bool operator >(Angle firstAngle, Angle secondAngle) => firstAngle.IsGreaterThan(secondAngle);

        /// <summary>
        /// Indicates whether a specified Angle is greater than another specified Angle.
        /// </summary>
        /// <param name="angle">The angle to compare.</param>
        /// <returns>true if the value of firstAngle is greater than the value of secondAngle; otherwise, false.</returns>
        public abstract bool IsGreaterThan(Angle angle);

        /// <summary>
        /// Indicates whether a specified Angle is greater than or equal to another specified Angle.
        /// </summary>
        /// <param name="firstAngle">The first angle to compare.</param>
        /// <param name="secondAngle">The second angle to compare.</param>
        /// <returns>true if the value of firstAngle is greater than or equal to the value of secondAngle; otherwise, false.</returns>
        public static bool operator >=(Angle firstAngle, Angle secondAngle) => firstAngle.IsGreaterThanOrEqual(secondAngle);

        /// <summary>
        /// Indicates whether a specified Angle is greater than or equal to another specified Angle.
        /// </summary>
        /// <param name="angle">The angle to compare.</param>
        /// <returns>true if the value of firstAngle is greater than or equal to the value of secondAngle; otherwise, false.</returns>
        public abstract bool IsGreaterThanOrEqual(Angle angle);

        #endregion

        #region Equality implementation

        /// <summary>
        /// Indicates a specified Angle is equal to another specified Angle.
        /// </summary>
        /// <param name="firstAngle">The first angle to compare.</param>
        /// <param name="secondAngle">The second angle to compare.</param>
        /// <returns>true if the value of firstAngle is equal to the value of secondAngle; otherwise, false.</returns>
        public static bool operator ==(Angle firstAngle, Angle secondAngle) => firstAngle.IsEqual(secondAngle);

        /// <summary>
        /// Indicates a specified Angle is equal to another specified Angle.
        /// </summary>
        /// <param name="angle">The angle to compare.</param>
        /// <returns>true if the value of firstAngle is equal to the value of secondAngle; otherwise, false.</returns>
        public abstract bool IsEqual(Angle angle);

        /// <summary>
        /// Indicates whether two Angle instances are not equal.
        /// </summary>
        /// <param name="firstAngle">The first angle to compare.</param>
        /// <param name="secondAngle">The second angle to compare.</param>
        /// <returns>true if the value of firstAngle is not equal to the value of secondAngle; otherwise, false.</returns>
        public static bool operator !=(Angle firstAngle, Angle secondAngle) => firstAngle.IsNotEqual(secondAngle);

        /// <summary>
        /// Indicates a specified Angle is not equal to another specified Angle.
        /// </summary>
        /// <param name="angle">The angle to compare.</param>
        /// <returns>true if the value of firstAngle is equal to the value of secondAngle; otherwise, false.</returns>
        public abstract bool IsNotEqual(Angle angle);

        #endregion

        #region Mod (%) Operator

        /// <summary>
        /// Remainder after division of one angle by another.
        /// </summary>
        /// <param name="divident">The divident angle.</param>
        /// <param name="divisor">The divisor angle.</param>
        /// <returns>Returns remainder after division of divident angle by divisor angle.</returns>
        public static double operator %(Angle divident, Angle divisor) => divident.Mod(divisor);

        /// <summary>
        /// Remainder after division of one angle by another.
        /// </summary>
        /// <param name="angle">The divisor angle.</param>
        /// <returns>Returns remainder after division of divident angle by divisor angle.</returns>
        public abstract double Mod(Angle angle);

        #endregion

        #region Trigonometric functions

        /// <summary>
        /// Return the sine of the specified angle.
        /// </summary>
        /// <returns>The sine of the specified angle. If angle is equal to NaN, NegativeInfinity, or PositiveInfinity, this method returns NaN.</returns>
        public double Sin() => Math.Sin(Value);

        /// <summary>
        /// Return the sine of the specified angle.
        /// </summary>
        /// <param name="angle">An angle.</param>
        /// <param name="result">The sine of the specified angle. If angle is equal to NaN, NegativeInfinity, or PositiveInfinity, this method returns NaN.</param>
        public static void Sin(ref Angle angle, out double result)
        {
            result = Math.Sin(angle.Value);
        }

        /// <summary>
        /// Return the cosine of the specified angle.
        /// </summary>
        /// <returns>The cosine of the specified angle. If angle is equal to NaN, NegativeInfinity, or PositiveInfinity, this method returns NaN.</returns>
        public double Cos() => Math.Cos(Value);

        /// <summary>
        /// Return the cosine of the specified angle.
        /// </summary>
        /// <param name="angle">An angle.</param>
        /// <param name="result">The cosine of the specified angle. If angle is equal to NaN, NegativeInfinity, or PositiveInfinity, this method returns NaN.</param>
        public static void Cos(ref Angle angle, out double result)
        {
            result = Math.Cos(angle.Value);
        }

        /// <summary>
        /// Returns the tangent of the specified angle.
        /// </summary>
        /// <returns>The tangent of the specified angle. If angle is equal to NaN, NegativeInfinity, or PositiveInfinity, this method returns NaN.</returns>
        public double Tan() => Math.Tan(Value);

        /// <summary>
        /// Returns the tangent of the specified angle.
        /// </summary>
        /// <param name="angle">An angle.</param>
        /// <param name="result">The tangent of the specified angle. If angle is equal to NaN, NegativeInfinity, or PositiveInfinity, this method returns NaN.</param>
        public static void Tan(ref Angle angle, out double result)
        {
            result = Math.Tan(angle.Value);
        }

        /// <summary>
        /// Returns the angle whose sine is the specified number.
        /// </summary>
        /// <param name="value">A number representing a sine, where value must be greater than or equal to -1, but less than or equal to 1.</param>
        /// <returns>The angle whose sine is the specified number.</returns>
        public abstract Angle Asin(double value);

        /// <summary>
        /// Returns the angle whose cosine is the specified number.
        /// </summary>
        /// <param name="value">A number representing a cosine, where value must be greater than or equal to -1, but less than or equal to 1.</param>
        /// <returns>The angle whose cosine is the specified number.</returns>
        public abstract Angle Acos(double value);

        /// <summary>
        /// Returns the angle whose tangent is the specified number.
        /// </summary>
        /// <param name="value">A number representing a tangent.</param>
        /// <returns>The angle whose tangent is the specified number.</returns>
        public abstract Angle Atan(double value);

        #endregion

        #region object overrides

        /// <summary>
        /// Returns a value indicating whether this instance is equal to a specified object.
        /// </summary>
        /// <param name="obj">An object to compare with this instance.</param>
        /// <returns>true if value is a Angle object that represents the same angle as the current Angle structure; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            var angle = obj as Angle;
            return angle != null &&
                   Value == angle.Value;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>A 32-bit signed integer hash code.</returns>
        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }


        #endregion

        #region Abstractions
       
        /// <summary>
        /// Returns double value of angle.
        /// </summary>
        /// <returns>Returns double.</returns>
        public virtual double ToDouble() => Value;

        /// <summary>
        /// Check anlge value is equal.
        /// </summary>
        /// <param name="obj">Source angle object.</param>
        /// <returns>Returns true if object values are equal, false otherwise.</returns>
        public bool Equals(Angle obj) => Value == obj.Value;

        #endregion

    }
}
