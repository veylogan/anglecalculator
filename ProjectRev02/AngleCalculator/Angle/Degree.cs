﻿using System;

namespace AngleCalculator
{
    /// <summary>
    /// Represents Degee unit.
    /// </summary>
    public class Degree : Angle
    {
        #region Constructror

        /// <summary>
        /// Degree constructor accepts double.
        /// </summary>
        /// <param name="value"></param>
        public Degree(double value) : base(value)
        {
            Value = Reduce(value);
        }

        #endregion

        #region Angle abstractions

        /// <summary>
        /// Divides a angle by a angle. 
        /// </summary>
        /// <param name="divisor">Divisor in double.</param>
        /// <returns>Result of the division.</returns>
        public override Angle Divide(double divisor)
        {
            if (divisor == 0) throw new DivideByZeroException($"Divisor should be greater than 0.");
            return new Degree(Value / divisor);
        }

        /// <summary>
        /// Multiplies a degree by an angle value.
        /// </summary>
        /// <param name="multiplicant">Multiplicant source double.</param>
        /// <returns>Result of the multiplication in degree.</returns>
        public override Angle Multiply(double multiplicant) => new Degree(Value * multiplicant);

        /// <summary>
        /// Adds two angles. 
        /// </summary>
        /// <param name="angle">Source angle.</param>
        /// <returns>Result of the addition.</returns>
        public override Angle Add(Angle angle) => new Degree(Value + ToDegree(angle).Value);

        /// <summary>
        /// Subtracts a angle from a angle.  
        /// </summary>
        /// <param name="angle">Source angle.</param>
        /// <returns>Result of the subtraction.</returns>
        public override Angle Subtract(Angle angle) => new Degree(Value - ToDegree(angle).Value);

        /// <summary>
        /// Indicates whether a specified Angle is less than another specified Angle.
        /// </summary>
        /// <param name="angle">The angle to compare.</param>
        /// <returns>true if the value of firstAngle is less than the value of secondAngle; otherwise, false.</returns>
        public override bool IsLessThan(Angle angle) => Value < ToDegree(angle).Value;

        /// <summary>
        /// Indicates whether a specified Angle is less than or equal to another specified Angle.
        /// </summary>
        /// <param name="angle">The angle to compare.</param>
        /// <returns>true if the value of firstAngle is less than or equal to the value of secondAngle; otherwise, false.</returns>
        public override bool IsLessThanOrEqual(Angle angle) => Value <= ToDegree(angle).Value;

        /// <summary>
        /// Indicates whether a specified Angle is greater than another specified Angle.
        /// </summary>
        /// <param name="angle">The angle to compare.</param>
        /// <returns>true if the value of firstAngle is greater than the value of secondAngle; otherwise, false.</returns>
        public override bool IsGreaterThan(Angle angle) => Value > ToDegree(angle).Value;

        /// <summary>
        /// Indicates whether a specified Angle is greater than or equal to another specified Angle.
        /// </summary>
        /// <param name="angle">The angle to compare.</param>
        /// <returns>true if the value of firstAngle is greater than or equal to the value of secondAngle; otherwise, false.</returns>
        public override bool IsGreaterThanOrEqual(Angle angle) => Value >= ToDegree(angle).Value;

        /// <summary>
        /// Indicates a specified Angle is equal to another specified Angle.
        /// </summary>
        /// <param name="angle">The angle to compare.</param>
        /// <returns>true if the value of firstAngle is equal to the value of secondAngle; otherwise, false.</returns>
        public override bool IsEqual(Angle angle) => Value == ToDegree(angle).Value;

        /// <summary>
        /// Indicates a specified Angle is not equal to another specified Angle.
        /// </summary>
        /// <param name="angle">The angle to compare.</param>
        /// <returns>true if the value of firstAngle is equal to the value of secondAngle; otherwise, false.</returns>
        public override bool IsNotEqual(Angle angle) => Value != ToDegree(angle).Value;

        /// <summary>
        /// Remainder after division of one angle by another.
        /// </summary>
        /// <param name="angle">The divisor angle.</param>
        /// <returns>Returns remainder after division of divident angle by divisor angle.</returns>
        public override double Mod(Angle angle) => Value % ToDegree(angle).Value;

        /// <summary>
        /// Returns the angle whose sine is the specified number.
        /// </summary>
        /// <param name="value">A number representing a sine, where value must be greater than or equal to -1, but less than or equal to 1.</param>
        public override Angle Asin(double value)
        {
            if (value < -1.0 || value > 1.0)
                throw new ArgumentOutOfRangeException(nameof(value), value, "Argument must be greater or equal to -1.0 and less or equal to 1.0.");
            return new Degree(Math.Asin(value) * Units.DegreeByRadians);
        }

        /// <summary>
        /// Returns the angle whose cosine is the specified number.
        /// </summary>
        /// <param name="value">A number representing a cosine, where value must be greater than or equal to -1, but less than or equal to 1.</param>
        /// <returns>The angle whose cosine is the specified number.</returns>
        public override Angle Acos(double value)
        {
            if (value < -1.0 || value > 1.0)
                throw new ArgumentOutOfRangeException(nameof(value), value, "Argument must be greater or equal to -1.0 and less or equal to 1.0.");

            return new Degree(Math.Acos(value) * Units.DegreeByRadians);
        }

        /// <summary>
        /// Returns the angle whose tangent is the specified number.
        /// </summary>
        /// <param name="value">A number representing a tangent.</param>
        /// <returns>The angle whose tangent is the specified number.</returns>
        public override Angle Atan(double value) => new Degree(Math.Atan(value) * Units.DegreeByRadians);

        /// <summary>
        /// To Double value.
        /// </summary>
        /// <returns>Returns double value of Degree.</returns>
        public override double ToDouble()
        {
            return Value;
        }
        #endregion

        #region Degree Utils

        /// <summary>
        /// Convert from angle to degree
        /// </summary>
        /// <param name="angle">Source angle.</param>
        /// <returns>Result in degree.</returns>
        public Angle ToDegree(Angle angle)
        {
            dynamic dynamicAngle = angle;
            return (Degree)dynamicAngle;
        }

        public static double Reduce(double value)
        {
           return value % Units.FullAngleDegree;
        }
        #endregion

        #region Cast

        /// <summary>
        /// Implicitly convert double to Degree.
        /// </summary>
        /// <param name="value">Double value.</param>
        public static implicit operator Degree(Double value)
        {
            return new Degree(Reduce(value));
        }

        /// <summary>
        /// Implicitly convert Degree to Double.
        /// </summary>
        /// <param name="degree">Degree object.</param>
        public static implicit operator Double(Degree degree)
        {
            return degree.ToDouble();
        }

        /// <summary>
        /// Explicitly convert Degree to Radian.
        /// </summary>
        /// <param name="degree">Degree object.</param>
        public static explicit operator Radian(Degree degree)
        {
            return new Radian(degree.Value * Units.RadianByDegrees);
        }
        #endregion
    }
}
