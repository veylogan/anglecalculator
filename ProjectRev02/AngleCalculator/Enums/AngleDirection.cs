﻿namespace AngleCalculator
{
    /// <summary>
    /// Represents AngleDirection enumerator
    /// </summary>
    public enum AngleDirection
    {
        /// <summary>
        /// x = 0, y > 0
        /// </summary>
        North,
        /// <summary>
        /// x > 0 and y > 0
        /// </summary>
        NorthEast,
        /// <summary>
        /// x >= 0 and y = 0
        /// </summary>
        East,
        /// <summary>
        /// x > 0 and y < 0
        /// </summary>
        SouthEast,
        /// <summary>
        /// x = 0, y < 0
        /// </summary>
        South,
        /// <summary>
        /// x < 0 , y < 0
        /// </summary>
        SouthWest,
        /// <summary>
        /// x < 0, y = 0
        /// </summary>
        West,
        /// <summary>
        /// x < 0, y > 0
        /// </summary>
        NorthWest,
    }
}
