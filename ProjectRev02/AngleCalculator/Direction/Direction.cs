﻿namespace AngleCalculator
{
    /// <summary>
    /// Represents Direction 
    /// </summary>
    public abstract class Direction
    {
        #region Property

        /// <summary>
        /// Angle Property
        /// </summary>
        public Angle Angle { get; set; }

        /// <summary>
        /// Degee unit of angle.
        /// </summary>
        public Degree Degree
        {
            get
            {
                dynamic dynamicAngle = Angle;
                return (Degree)dynamicAngle;
            }
        }

        /// <summary>
        /// Radian unit of angle.
        /// </summary>
        public Radian Radian
        {
            get
            {
                dynamic dynamicAngle = Angle;
                return (Radian)dynamicAngle;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor with angle input.
        /// </summary>
        /// <param name="angle">Source angle.</param>
        public Direction(Angle angle) => this.Angle = angle;

        #endregion

        #region Quadrant

        /// <summary>
        /// Get quadrant of angle
        /// </summary>
        /// <returns>Returns quadrant of angle.</returns>
        public abstract Quadrant GetQuadrant();
        #endregion

        #region Direction

        /// <summary>
        /// Get direction of angle
        /// </summary>
        /// <returns>Returns direction of angle.</returns>
        public abstract AngleDirection GetDirection();
        #endregion

        #region Direction Utils

        /// <summary>
        /// Reduce degree value.
        /// </summary>
        /// <param name="value">Degree value in double.</param>
        /// <returns>Returns reduced degree value.</returns>
        public double Reduce(double value) => value % Units.FullAngleDegree < 0 ? value + Units.FullAngleDegree : value;

        /// <summary>
        /// Return Direction angle conversion.
        /// </summary>
        /// <param name="value">Degree double value.</param>
        /// <returns>Returns converted angle.</returns>
        public static double DirectionAngle(double value) => (Units.DirectionRatioDegree - value) % Units.FullAngleDegree;
        #endregion
    }
}
