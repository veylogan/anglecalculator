﻿namespace AngleCalculator
{
    /// <summary>
    /// Represents Compass direction (Angle from the positive Y-axis measured clockwise).
    /// </summary>
    public class Compass : Direction
    {
        #region Constructor
        /// <summary>
        /// Compass constructor accepting Angle.
        /// </summary>
        /// <param name="angle">Source angle object.</param>
        public Compass(Angle angle) : base(angle)
        {

        }

        #endregion

        #region Direction abstractions

        /// <summary>
        /// Get quadrant of angle from Y-axis.
        /// </summary>
        /// <returns>Returns quadrant.</returns>
        public override Quadrant GetQuadrant()
        {
            double degree = Reduce(Degree.Value);

            return (degree < Units.RightAngleDegree) ? Quadrant.First :
                (degree < Units.StraightAngleDegree) ? Quadrant.Fourth :
                (degree < Units.StraightAngleDegree + Units.RightAngleDegree) ? Quadrant.Third : Quadrant.Second;
        }

        /// <summary>
        /// Get direction of angle.
        /// </summary>
        /// <returns>Returns direction.</returns>
        public override AngleDirection GetDirection()
        {
            double degree = Reduce(Degree.Value);

            return (degree == Units.InitDegree || degree == Units.FullAngleDegree) ? AngleDirection.North :
                (degree < Units.RightAngleDegree) ? AngleDirection.NorthEast :
                (degree == Units.RightAngleDegree) ? AngleDirection.East :
                (degree < Units.StraightAngleDegree) ? AngleDirection.SouthEast :
                (degree == Units.StraightAngleDegree) ? AngleDirection.South :
                (degree < Units.StraightAngleDegree + Units.StraightAngleDegree) ? AngleDirection.SouthWest :
                (degree == Units.StraightAngleDegree + Units.StraightAngleDegree) ? AngleDirection.West :
                (degree < Units.FullAngleDegree) ? AngleDirection.NorthWest : AngleDirection.North;
        }


        #endregion

        #region Cast

        /// <summary>
        /// Explicitly convert Compass to Mathematical.
        /// </summary>
        /// <param name="compass">Compass object.</param>
        public static explicit operator Mathematical(Compass compass)
        {
            return new Mathematical(new Degree(DirectionAngle(compass.Degree.Value)));
        }
        #endregion
    }
}
