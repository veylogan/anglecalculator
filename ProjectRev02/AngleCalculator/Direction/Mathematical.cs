﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngleCalculator
{
    /// <summary>
    /// Represents Mathematical direction (Angle from the positive X-axis measured counter clockwise).
    /// </summary>
    public class Mathematical : Direction
    {
        #region Constructor
        /// <summary>
        /// Mathematical constructor accepting angle.
        /// </summary>
        /// <param name="angle">Source angle object.</param>
        public Mathematical(Angle angle) : base(angle)
        {

        }
        #endregion

        #region Direction abstractions
        /// <summary>
        /// Get quadrant of angle from X-axis.
        /// </summary>
        /// <returns>Returns quadrant.</returns>
        public override Quadrant GetQuadrant()
        {
            double degree = Reduce(Degree.Value);
            return (degree < Units.RightAngleDegree) ? Quadrant.First :
                (degree < Units.StraightAngleDegree) ? Quadrant.Second :
                (degree < Units.StraightAngleDegree + Units.RightAngleDegree) ? Quadrant.Third : Quadrant.Fourth;
        }

        /// <summary>
        /// Get direction of angle.
        /// </summary>
        /// <returns>Returns direction.</returns>
        public override AngleDirection GetDirection()
        {
            var degree = Degree.Value % Units.FullAngleDegree < 0 ? Degree.Value + Units.FullAngleDegree : Degree.Value;

            return (degree == Units.InitDegree || degree == Units.FullAngleDegree) ? AngleDirection.East :
                (degree < Units.RightAngleDegree) ? AngleDirection.NorthEast :
                (degree == Units.RightAngleDegree) ? AngleDirection.North :
                (degree < Units.StraightAngleDegree) ? AngleDirection.NorthWest :
                (degree == Units.StraightAngleDegree) ? AngleDirection.West :
                (degree < Units.StraightAngleDegree + Units.StraightAngleDegree) ? AngleDirection.SouthWest :
                (degree == Units.StraightAngleDegree + Units.StraightAngleDegree) ? AngleDirection.South :
                (degree < Units.FullAngleDegree) ? AngleDirection.SouthEast : AngleDirection.East;
        }

        #endregion

        #region Cast

        /// <summary>
        /// Explicitly convert Mathematical to Compass.
        /// </summary>
        /// <param name="mathematical">Mathematical object.</param>
        public static explicit operator Compass(Mathematical mathematical)
        {
            return new Compass(new Degree(DirectionAngle(mathematical.Degree.Value)));
        }

        #endregion
    }
}
