﻿using Xunit;

namespace AngleCalculator.UnitTests
{
    /// <summary>
    /// Represents Angle Comparer  Tests 
    /// Arranging data preparation done through InitialData attribute. 
    /// </summary>
    public class AngleComparerTests
    {
        #region GreaterThan
        
        [Theory]
        [InlineData(40, 30, true)]
        [InlineData(100, 90, true)]
        public void VerifyGreaterThanOperatorBetweenDegreeAndDegree(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Degree(left);
            var angleRight = new Degree(right);
            //Act
            var actual = angleLeft > angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(300, 5, true)]
        [InlineData(70, 1, true)]
        public void VerifyGreaterThanOperatorBetweenDegreeAndRadian(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Degree(left);
            var angleRight = new Radian(right);
            //Act
            var actual = angleLeft > angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(3, 2, true)]
        [InlineData(2, 1, true)]
        public void VerifyGreaterThanOperatorBetweenRadianAndRadian(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Radian(left);
            var angleRight = new Radian(right);
            //Act
            var actual = angleLeft > angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(40, 30, true)]
        [InlineData(100, 90, true)]
        public void VerifyGreaterThanOperatorBetweenRadianAndDegree(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Radian(left);
            var angleRight = new Degree(right);
            //Act
            var actual = angleLeft > angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }
        #endregion

        #region LessThan

        
        [Theory]
        [InlineData(30, 40, true)]
        [InlineData(90, 100, true)]
        public void VerifyLessThanOperatorBetweenDegreeAndDegree(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Degree(left);
            var angleRight = new Degree(right);
            //Act
            var actual = angleLeft < angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(5, 300, true)]
        [InlineData(1, 70, true)]
        public void VerifyLessThanOperatorBetweenDegreeAndRadian(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Degree(left);
            var angleRight = new Radian(right);
            //Act
            var actual = angleLeft < angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(1, 2, true)]
        [InlineData(2, 3, true)]
        [InlineData(1, 0.5, false)]
        [InlineData(2, 1.8, false)]
        public void VerifyLessThanOperatorBetweenRadianAndRadian(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Radian(left);
            var angleRight = new Radian(right);
            //Act
            var actual = angleLeft < angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(1, 150, true)]
        [InlineData(0.5, 100, true)]
        public void VerifyLessThanOperatorBetweenRadianAndDegree(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Radian(left);
            var angleRight = new Degree(right);
            //Act
            var actual = angleLeft < angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }

        #endregion

        #region LessThanOrEqual

        [Theory]
        [InlineData(30, 40, true)]
        [InlineData(100, 100, true)]
        public void VerifyLessThanOrEqualOperatorBetweenDegreeAndDegree(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Degree(left);
            var angleRight = new Degree(right);
            //Act
            var actual = angleLeft <= angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(5, 300, true)]
        [InlineData(1, 70, true)]
        public void VerifyLessThanOrEqualOperatorBetweenDegreeAndRadian(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Degree(left);
            var angleRight = new Radian(right);
            //Act
            var actual = angleLeft <= angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(1, 1, true)]
        [InlineData(3.1, 3.1, true)]
        [InlineData(3, 3.1, true)]
        [InlineData(2.8, 3, true)]
        [InlineData(1, 1.5, true)]
        public void VerifyLessThanOrEqualOperatorBetweenRadianAndRadian(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Radian(left);
            var angleRight = new Radian(right);
            //Act
            var actual = angleLeft <= angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(1, 150, true)]
        [InlineData(0.5, 100, true)]
        public void VerifyLessThanOrEqualOperatorBetweenRadianAndDegree(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Radian(left);
            var angleRight = new Degree(right);
            //Act
            var actual = angleLeft <= angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }

        #endregion

        #region GreaterThanOrEqual

        [Theory]
        [InlineData(40, 30, true)]
        [InlineData(90, 90, true)]
        public void VerifyGreaterThanOrEqualOperatorBetweenDegreeAndDegree(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Degree(left);
            var angleRight = new Degree(right);
            //Act
            var actual = angleLeft >= angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(300, 5, true)]
        [InlineData(70, 1, true)]
        public void VerifyGreaterThanOrEqualOperatorBetweenDegreeAndRadian(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Degree(left);
            var angleRight = new Radian(right);
            //Act
            var actual = angleLeft >= angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(2, 2, true)]
        [InlineData(3, 2, true)]
        [InlineData(3, 3.1, false)]
        [InlineData(1, 2, false)]
        public void VerifyGreaterThanOrEqualOperatorBetweenRadianAndRadian(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Radian(left);
            var angleRight = new Radian(right);
            //Act
            var actual = angleLeft >= angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(40, 30, true)]
        [InlineData(100, 90, true)]
        public void VerifyGreaterThanOrEqualOperatorBetweenRadianAndDegree(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Radian(left);
            var angleRight = new Degree(right);
            //Act
            var actual = angleLeft >= angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }


        #endregion

        #region Equal

        
        [Theory]
        [InlineData(30, 30, true)]
        [InlineData(90, 90, true)]
        public void VerifyEqualOperatorBetweenDegreeAndDegree(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Degree(left);
            var angleRight = new Degree(right);
            //Act
            var actual = angleLeft == angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(286.47889756541161, 5, true)]
        [InlineData(57.295779513082323, 1, true)]
        public void VerifyEqualOperatorBetweenDegreeAndRadian(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Degree(left);
            var angleRight = new Radian(right);
            //Act
            var actual = angleLeft == angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(30, 30, true)]
        [InlineData(90, 90, true)]
        public void VerifyEqualOperatorBetweenRadianAndRadian(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Radian(left);
            var angleRight = new Radian(right);
            //Act
            var actual = angleLeft == angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(1.0471975511965976, 60, true)]
        [InlineData(1.5707963267948966, 90, true)]
        public void VerifyEqualOperatorBetweenRadianAndDegree(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Radian(left);
            var angleRight = new Degree(right);
            //Act
            var actual = angleLeft == angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }
        #endregion

        #region NotEqual

        [Theory]
        [InlineData(20, 30, true)]
        [InlineData(80, 90, true)]
        public void VerifyNotEqualOperatorBetweenDegreeAndDegree(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Degree(left);
            var angleRight = new Degree(right);
            //Act
            var actual = angleLeft != angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(5, 5, true)]
        [InlineData(1, 1, true)]
        public void VerifyNotEqualOperatorBetweenDegreeAndRadian(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Degree(left);
            var angleRight = new Radian(right);
            //Act
            var actual = angleLeft != angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(20, 30, true)]
        [InlineData(80, 90, true)]
        public void VerifyNotEqualOperatorBetweenRadianAndRadian(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Radian(left);
            var angleRight = new Radian(right);
            //Act
            var actual = angleLeft != angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(60, 60, true)]
        [InlineData(90, 90, true)]
        public void VerifyNotEqualOperatorBetweenRadianAndDegree(double left, double right, bool expected)
        {
            //Arrange
            var angleLeft = new Radian(left);
            var angleRight = new Degree(right);
            //Act
            var actual = angleLeft != angleRight;

            //Assert
            Assert.Equal(expected, actual);
        }

        #endregion
    }
}
