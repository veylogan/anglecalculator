﻿using System;
using Xunit;

namespace AngleCalculator.UnitTests
{
    /// <summary>
    /// Represents Angle Basic Mathematics Tests 
    /// Arranging data preparation done through InitialData attribute. 
    /// </summary>
    public class AngleBasicMathematicsTests
    {
        #region Division


        [Theory]
        [InlineData(30, 5, 6.0)]
        [InlineData(60, 10, 6)]
        public void VerifyDividDegreeByDouble(double dividend, double divisor, double expected)
        {
            //Act
            var angle = new Degree(dividend) / divisor;

            //Assert
            Assert.Equal(expected, angle.Value);
        }

        [Theory]
        [InlineData(1.0, 0.5, 2.0)]
        [InlineData(2.0, 0.5, 4.0)]
        public void VerifyDividRadianByDouble(double dividend, double divisor, double expected)
        {
            //Act
            var angle = new Radian(dividend) / divisor;

            //Assert
            Assert.Equal(expected, Math.Round(angle.Value, 3));
        }

        #endregion

        #region Multiplication


        [Theory]
        [InlineData(30, 5.0, 150.0)]
        [InlineData(60, 3.0, 180.0)]
        public void VerifyMultipyDegeeWithDouble(double multiplier, double multiplicant, double expected)
        {
            //Act
            var angle = new Degree(multiplier) * multiplicant;

            //Assert
            Assert.Equal(expected, Math.Round(angle.Value, 3));
        }

        [Theory]
        [InlineData(2.0, 5.0, 3.717)]
        [InlineData(3.0, 3.0, 2.717)]
        public void VerifyMultipyRadianWithDouble(double multiplier, double multiplicant, double expected)
        {
            //Act
            var angle = new Radian(multiplier) * multiplicant;

            //Assert
            Assert.Equal(expected, Math.Round(angle.Value, 3));
        }

       
        #endregion

        #region Addition


        [Theory]
        [InlineData(5.0, 5.0, 10.0)]
        [InlineData(10.0, 3.0, 13.0)]
        public void VerifyAdditionOfDegeeWithDegree(double left, double right, double expected)
        {
            //Act

            var angle = new Degree(left) + new Degree(right);

            //Assert
            Assert.Equal(expected, Math.Round(angle.Value, 3));
        }

        [Theory]
        [InlineData(2.0, 5.0, 288.479)]
        [InlineData(10.0, 3.0, 181.887)]
        public void VerifyAdditionOfDegeeWithRadian(double left, double right, double expected)
        {
            //Act
            var angle = new Degree(left) + new Radian(right);

            //Assert
            Assert.Equal(expected, Math.Round(angle.Value, 3));
        }

        [Theory]
        [InlineData(2.0, 15.0, 2.262)]
        [InlineData(1.0, 30.0, 1.524)]
        public void VerifyAdditionOfRadianWithDegree(double left, double right, double expected)
        {
            //Act
            var angle = new Radian(left) + new Degree(right);

            //Assert
            Assert.Equal(expected, Math.Round(angle.Value, 3));
        }
        #endregion

        #region Subtraction

        [Theory]
        [InlineData(2.0, 5.0, -3.0)]
        [InlineData(10.0, 3.0, 7.0)]
        public void VerifySubtractionOfDegeeWithDegree(double left, double right, double expected)
        {
            //Act
            var angle = new Degree(left) - new Degree(right);

            //Assert
            Assert.Equal(expected, Math.Round(angle.Value, 3));
        }

        [Theory]
        [InlineData(2.0, 5.0, -284.479)]
        [InlineData(10.0, 3.0, -161.887)]
        public void VerifySubtractionOfDegeeWithRadian(double left, double right, double expected)
        {
            //Act
            var angle = new Degree(left) - new Radian(right);

            //Assert
            Assert.Equal(expected, Math.Round(angle.Value, 3));
        }

        [Theory]
        [InlineData(2.0, 5.0, 1.913)]
        [InlineData(1.5, 30.0, 0.976)]
        public void VerifySubtractionOfRadianWithDegree(double left, double right, double expected)
        {
            //Act
            var angle = new Radian(left) - new Degree(right);

            //Assert
            Assert.Equal(expected, Math.Round(angle.Value, 3));
        }

        #endregion

        #region Modulo


        [Theory]
        [InlineData(2.0, 5.0, 2.0)]
        [InlineData(10.0, 3.0, 10.0)]
        public void VerifyModOfDegeeWithRadian(double left, double right, double expected)
        {
            //Act
            var actual = new Degree(left) % new Radian(right);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(2.0, 5.0, 0.080137823)]
        [InlineData(10.0, 3.0, 0.051623264)]
        public void VerifyModOfRadianWithDegree(double left, double right, double expected)
        {
            //Act
            var actual = new Radian(left) % new Degree(right);

            //Assert
            Assert.Equal(expected, Math.Round(actual, 9));
        }
        [Theory]
        [InlineData(2.0, 5.0, 2)]
        [InlineData(10.0, 3.0, 1)]
        public void VerifyModOfDegeeWithDegree(double left, double right, double expected)
        {
            //Act
            var actual = new Degree(left) % new Degree(right);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(2.0, 5.0, 2.0)]
        [InlineData(1.0, 3.0, 1.0)]
        public void VerifyModOfRadianWithRadian(double left, double right, double expected)
        {
            //Act
            var actual = new Radian(left) % new Radian(right);

            //Assert
            Assert.Equal(expected, actual);
        }
        #endregion
    }
}
