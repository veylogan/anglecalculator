﻿using System;
using Xunit;

namespace AngleCalculator.UnitTests
{
    /// <summary>
    /// Represents Angle Type ConversionTests
    /// Arranging data done through InitialData attribute. 
    /// </summary>
    public class AngleTypeConversionTests
    {
        #region Type Explicit Cast

        [Theory]
        [InlineData(30, 0.523598776)]
        [InlineData(45, 0.785398163)]
        [InlineData(60, 1.047197551)]
        public void VerifyCanConvertDegeeAsRadian(double input, double expected)
        {
            //Arrange
            var degree = new Degree(input);

            //Act
            var radian = (Radian)degree;
            var actual = Math.Round(radian.Value, 9);

            //Assert
            Assert.Equal(expected, actual);

        }

        [Theory]
        [InlineData(1, 57.295780)]
        [InlineData(2, 114.591559)]
        [InlineData(3, 171.887339)]
        public void VerifyCanConvertRadianAsDegree(double input, double expected)
        {
            //Arrange
            var radian = new Radian(input);

            //Act
            var degree = (Degree)radian;
            var actual = Math.Round(degree.Value, 6);

            //Assert
            Assert.Equal(expected, actual);
        }


        #endregion
    }
}
