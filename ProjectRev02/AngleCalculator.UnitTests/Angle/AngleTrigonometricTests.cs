﻿using System;
using Xunit;

namespace AngleCalculator.UnitTests
{
    /// <summary>
    /// Represents Angle Trigonometric Tests 
    /// Arranging data done through InitialData attribute. 
    /// </summary>
    public class AngleTrigonometricTests
    {
        #region Sin

        [Theory]
        [InlineData(30, -0.988)]
        [InlineData(90, 0.894)]
        public void VerifySinOfDegree(double degree, double expected)
        {
            //Arrange
            var angle = new Degree(degree);
            //Act
            var sineActual = angle.Sin();

            //Assert
            Assert.Equal(expected, Math.Round(sineActual, 3));
        }

        [Theory]
        [InlineData(30, -0.988)]
        [InlineData(90, 0.894)]
        public void VerifySinOfRadian(double radian, double expected)
        {
            //Arrange
            var angle = new Radian(radian);
            //Act
            var sineActual = angle.Sin();

            //Assert
            Assert.Equal(expected, Math.Round(sineActual, 3));
        }
        #endregion

        #region Cos

        [Theory]
        [InlineData(0, 1)]
        [InlineData(90, -0.448)]
        public void VerifyCosOfRadian(double radian, double expected)
        {
            //Arrange
            var angle = new Radian(radian);
            //Act
            var sineActual = angle.Cos();

            //Assert
            Assert.Equal(expected, Math.Round(sineActual, 3));
        }

        [Theory]
        [InlineData(0, 1)]
        [InlineData(90, -0.448)]
        public void VerifyCosOfDegree(double degree, double expected)
        {
            //Arrange
            var angle = new Degree(degree);
            //Act
            var sineActual = angle.Cos();

            //Assert
            Assert.Equal(expected, Math.Round(sineActual, 3));
        }

        #endregion

        #region Tan

        [Theory]
        [InlineData(0, 0)]
        [InlineData(90, -1.995)]
        public void VerifyTanOfRadian(double radian, double expected)
        {
            //Arrange
            var angle = new Radian(radian);
            //Act
            var sineActual = angle.Tan();

            //Assert
            Assert.Equal(expected, Math.Round(sineActual, 3));
        }

        [Theory]
        [InlineData(0, 0)]
        [InlineData(90, -1.995)]
        public void VerifyTanOfDegree(double degree, double expected)
        {
            //Arrange
            var angle = new Degree(degree);
            //Act
            var sineActual = angle.Tan();

            //Assert
            Assert.Equal(expected, Math.Round(sineActual, 3));
        }

        #endregion

        #region ArcSin

        [Theory]
        [InlineData(30, 0.1, 0.100167421)]
        [InlineData(30, 0.2, 0.201357921)]
        [InlineData(30, 0.3, 0.304692654)]
        public void VerifyArcSinOfRadian(double radian, double asinInput, double expected)
        {
            //Arrange
            var angle = new Radian(radian);
            //Act
            var arcSineActual = angle.Asin(asinInput);

            //Assert
            Assert.Equal(expected, Math.Round(arcSineActual.Value, 9));
        }

        [Theory]
        [InlineData(30, 0.1, 5.739170477)]
        [InlineData(30, 0.2, 11.536959033)]
        [InlineData(30, 0.3, 17.457603124)]
        public void VerifyArcSinOfDegree(double degree, double asinInput, double expected)
        {
            //Arrange
            var angle = new Degree(degree);
            //Act
            var arcSineActual = angle.Asin(asinInput);

            //Assert
            Assert.Equal(expected, Math.Round(arcSineActual.Value, 9));
        }
        #endregion

        #region ArcCos

        [Theory]
        [InlineData(30, 0.1, 1.470628906)]
        [InlineData(30, 0.2, 1.369438406)]
        [InlineData(30, 0.3, 1.266103673)]
        public void VerifyArcCosOfRadian(double radian, double asinInput, double expected)
        {
            //Arrange
            var angle = new Radian(radian);
            //Act
            var arcSineActual = angle.Acos(asinInput);

            //Assert
            Assert.Equal(expected, Math.Round(arcSineActual.Value, 9));
        }

        [Theory]
        [InlineData(30, 0.1, 84.260829523)]
        [InlineData(30, 0.2, 78.463040967)]
        [InlineData(30, 0.3, 72.542396876)]
        public void VerifyArcCosOfDegree(double degree, double asinInput, double expected)
        {
            //Arrange
            var angle = new Degree(degree);
            //Act
            var arcSineActual = angle.Acos(asinInput);

            //Assert
            Assert.Equal(expected, Math.Round(arcSineActual.Value, 9));
        }
        #endregion

        #region ArcTan

        [Theory]
        [InlineData(30, 0.1, 0.099668652)]
        [InlineData(30, 0.2, 0.19739556)]
        [InlineData(30, 0.3, 0.291456794)]
        public void VerifyArcTanOfRadian(double radian, double asinInput, double expected)
        {
            //Arrange
            var angle = new Radian(radian);
            //Act
            var arcSineActual = angle.Atan(asinInput);

            //Assert
            Assert.Equal(expected, Math.Round(arcSineActual.Value, 9));
        }

        [Theory]
        [InlineData(30, 0.1, 5.710593137)]
        [InlineData(30, 0.2, 11.309932474)]
        [InlineData(30, 0.3, 16.699244234)]
        public void VerifyArcTanOfDegree(double degree, double asinInput, double expected)
        {
            //Arrange
            var angle = new Degree(degree);
            //Act
            var arcSineActual = angle.Atan(asinInput);

            //Assert
            Assert.Equal(expected, Math.Round(arcSineActual.Value, 9));
        }

        #endregion
    }
}
