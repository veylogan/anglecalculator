﻿using System;
using Xunit;

namespace AngleCalculator.UnitTests
{
    public class DirectionTypeConversionTests
    {
        #region Compass to Mathematical

        [Theory]
        [InlineData(30, 60)]
        [InlineData(0, 90)]
        [InlineData(90, 0)]
        [InlineData(180, 270)]
        [InlineData(270, 180)]
        public void VerifyCanConvertCompassToMathematicalByDegree(double degreeInput, double expected)
        {
            //Arrange
            var degree = new Degree(degreeInput);
            var compass = new Compass(degree);

            //Act
            var mathematical = (Mathematical)compass;
            var actual = mathematical.Angle.Value;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(0, 90)]
        [InlineData(0.523598776, 60)]
        [InlineData(1.57079633, 360)]
        public void VerifyCanConvertCompassToMathematicalByRadian(double radianInput, double expected)
        {
            //Arrange
            var radian = new Radian(radianInput);
            var compass = new Compass(radian);

            //Act
            var mathematical = (Mathematical)compass;
            var actual = Math.Round(mathematical.Angle.Value);

            //Assert
            Assert.Equal(expected, actual);
        }
        #endregion

        #region Mathematical to Compass

        [Theory]
        [InlineData(30, 60)]
        [InlineData(0, 90)]
        [InlineData(90, 0)]
        [InlineData(180, 270)]
        [InlineData(270, 180)]
        public void VerifyCanConvertMathematicalToCompassByDegree(double degreeInput, double expected)
        {
            //Arrange
            var degree = new Degree(degreeInput);
            var mathematical = new Mathematical(degree);

            //Act
            var compass = (Compass)mathematical;
            var actual = compass.Angle.Value;

            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(0, 90)]
        [InlineData(0.523598776, 60)]
        [InlineData(1.57079633, 360)]
        public void VerifyCanConvertMathematicalToCompassByRadian(double radianInput, double expected)
        {
            //Arrange
            var radian = new Radian(radianInput);
            var mathematical = new Mathematical(radian);

            //Act
            var compass = (Compass)mathematical;
            var actual = Math.Round(compass.Angle.Value);

            //Assert
            Assert.Equal(expected, actual);
        }
        #endregion
    }
}
