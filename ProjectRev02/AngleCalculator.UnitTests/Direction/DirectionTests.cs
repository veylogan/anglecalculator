﻿using Xunit;

namespace AngleCalculator.UnitTests
{
    /// <summary>
    /// Represents Direction Tests
    /// 
    /// </summary>
    public class DirectionTests
    {
        #region Quadrant

        [Theory]
        [InlineData(0, 0)]
        [InlineData(10, 0)]
        [InlineData(90, 3)]
        [InlineData(120, 3)]
        [InlineData(180, 2)]
        [InlineData(270, 1)]
        [InlineData(360, 0)]
        [InlineData(359, 1)]
        public void VerifyQuadrantInCompassAndDegree(double input, double expected)
        {
            //Arrange
            var degree = new Degree(input);
            var compass = new Compass(degree);

            //Act
            var quadrant = (int)compass.GetQuadrant();

            //Assert
            Assert.Equal(expected, quadrant);
        }


        [Theory]
        [InlineData(0, 0)]
        [InlineData(10, 0)]
        [InlineData(90, 1)]
        [InlineData(120, 1)]
        [InlineData(180, 2)]
        [InlineData(270, 3)]
        [InlineData(360, 0)]
        [InlineData(359, 3)]
        public void VerifyQuadrantInMathematicalAndDegree(double input, double expected)
        {
            //Arrange
            var degree = new Degree(input);
            var mathematical = new Mathematical(degree);

            //Act
            var quadrant = (int)mathematical.GetQuadrant();

            //Assert
            Assert.Equal(expected, quadrant);
        }

        [Theory]
        [InlineData(0, 0)]
        [InlineData(1, 0)]
        [InlineData(2, 3)]
        [InlineData(3, 3)]
        [InlineData(4, 2)]
        [InlineData(5, 1)]
        [InlineData(6, 1)]
        [InlineData(7, 0)]
        public void VerifyQuadrantInCompassAndRadian(double input, double expected)
        {
            //Arrange
            var radian = new Radian(input);
            var compass = new Compass(radian);

            //Act
            var quadrant = (int)compass.GetQuadrant();

            //Assert
            Assert.Equal(expected, quadrant);
        }

        [Theory]
        [InlineData(0, 0)]
        [InlineData(1, 0)]
        [InlineData(2, 1)]
        [InlineData(3, 1)]
        [InlineData(4, 2)]
        [InlineData(5, 3)]
        [InlineData(6, 3)]
        [InlineData(7, 0)]
        public void VerifyQuadrantInMathematicalAndRadian(double input, double expected)
        {
            //Arrange
            var radian = new Radian(input);
            var mathematical = new Mathematical(radian);

            //Act
            var quadrant = (int)mathematical.GetQuadrant();

            //Assert
            Assert.Equal(expected, quadrant);
        }

        #endregion

        #region Direction

        [Theory]
        [InlineData(0, 0)]
        [InlineData(10, 1)]
        [InlineData(90, 2)]
        [InlineData(120, 3)]
        [InlineData(180, 4)]
        [InlineData(390, 1)]
        public void VerifyDirectionOfCompassAndDegree(double input, double expected)
        {
            //Arrange
            var degree = new Degree(input);
            var compass = new Compass(degree);

            //Act
            var quadrant = (int)compass.GetDirection();

            //Assert
            Assert.Equal(expected, quadrant);
        }


        [Theory]
        [InlineData(0, 2)]
        [InlineData(10, 1)]
        [InlineData(90, 0)]
        [InlineData(120, 7)]
        [InlineData(180, 6)]
        [InlineData(390, 1)]
        public void VerifyDirectionOfMathematicalAndDegree(double input, double expected)
        {
            //Arrange
            var degree = new Degree(input);
            var mathematical = new Mathematical(degree);

            //Act
            var quadrant = (int)mathematical.GetDirection();

            //Assert
            Assert.Equal(expected, quadrant);
        }

        [Theory]
        [InlineData(0, 0)]
        [InlineData(1, 1)]
        [InlineData(1.5, 1)]
        [InlineData(2, 3)]
        [InlineData(2.5, 3)]
        [InlineData(3, 3)]
        [InlineData(3.5, 5)]
        [InlineData(4, 5)]
        [InlineData(4.5, 5)]
        [InlineData(5, 5)]
        [InlineData(5.5, 5)]
        [InlineData(6, 5)]
        [InlineData(6.1, 5)]
        [InlineData(6.5, 1)]
        [InlineData(7, 1)]
        [InlineData(7.5, 1)]
        public void VerifyDirectionOfCompassAndRadian(double input, double expected)
        {
            //Arrange
            var radian = new Radian(input);
            var compass = new Compass(radian);

            //Act
            var quadrant = (int)compass.GetDirection();

            //Assert
            Assert.Equal(expected, quadrant);
        }


        [Theory]
        [InlineData(0, 2)]
        [InlineData(1, 1)]
        [InlineData(1.5, 1)]
        [InlineData(2, 7)]
        [InlineData(2.5, 7)]
        [InlineData(3, 7)]
        public void VerifyDirectionOfMathematicalAndRadian(double input, double expected)
        {
            //Arrange
            var radian = new Radian(input);
            var mathematical = new Mathematical(radian);

            //Act
            var quadrant = (int)mathematical.GetDirection();

            //Assert
            Assert.Equal(expected, quadrant);
        }
        #endregion

    }
}
